﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TAP_U2P1
{
    
    public partial class Form1 : Form
    {
        private int segundo = 0, seg =0;
        public Form1()
        {
            InitializeComponent();

            //Thread thread1 = new Thread(funcionHilo1);
            //thread1.Start();

            //Thread thread2 = new Thread(funcionHilo2);
            //thread2.Start();

            timer1.Start();
            timer2.Start();
            
            
            
            
        }

        private void funcionHilo1()
        {
            while (true)
            {
                Console.WriteLine("Soy el hilo 1");
                Thread.Sleep(500);
            }
        }

        private void funcionHilo2()
        {
            for (; ; )
            {
                Console.WriteLine("Soy el hilio 2 ");
                Thread.Sleep(500);

            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = "Segundo: " + segundo;
            segundo++;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            if(flowLayoutPanel1.Width > 0)
            {
                flowLayoutPanel1.Size = new Size(flowLayoutPanel1.Width - 1, flowLayoutPanel1.Height);
            }
        }
        
        

        private void pictureBox4_Click(object sender, EventArgs e)
        {

        }
        

        private void timer3_Tick(object sender, EventArgs e)
        {
            if (seg == 1)
                labelRun.Image = imageList1.Images[1];
            if (seg == 2)
                labelRun.Image = imageList1.Images[2];
            if (seg == 3)
                labelRun.Image = imageList1.Images[3];
            if (seg == 4)
                labelRun.Image = imageList1.Images[4];

            if(seg == 5)
                labelRun.Image = imageList1.Images[0];
            if(seg == 6)
                labelRun.Image = imageList1.Images[1];
            if (seg == 7)
                labelRun.Image = imageList1.Images[2];
            if (seg == 8)
                labelRun.Image = imageList1.Images[3];
            if (seg == 9)
                labelRun.Image = imageList1.Images[4];

            else if (seg == 10)
                seg = 0;
            seg++;

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void timer4_Tick(object sender, EventArgs e)
        {

            if (seg == 3)
                labelJump.Image = imageList2.Images[1];
            if (seg == 6)
                labelJump.Image = imageList2.Images[2];
            if (seg == 9)
                labelJump.Image = imageList2.Images[3];
            if (seg == 12)
                labelJump.Image = imageList2.Images[4];
            if (seg == 15)
                labelJump.Image = imageList2.Images[5];
            if (seg == 18)
                labelJump.Image = imageList2.Images[1];
            if (seg == 21)
                labelJump.Image = imageList2.Images[0];

            else if (seg == 23)
                seg = 0;
            seg++;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer4.Start();
        }

        private void buttonRun_Click(object sender, EventArgs e)
        {
            timer3.Start();
        }
    }
}
